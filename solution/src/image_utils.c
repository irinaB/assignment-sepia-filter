#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/image_utils.h"
#include "../include/sepia.h"

unsigned char sat( uint64_t x) {
    if (x < 256)
        return x;
    return 255;
}

void sepia_one_pixel(image* old_image, image* image, uint32_t x, uint32_t y) {
    pixel no_filter = image_get_pixel(old_image, (point){x, y});
    pixel sepia_filter = image_get_pixel(image, (point){x, y});

    sepia_filter.r = sat((float)no_filter.r * .393f + (float)no_filter.g * .769f + (float)no_filter.b * .189f);
    sepia_filter.g = sat((float)no_filter.r * .349f + (float)no_filter.g * .686f + (float)no_filter.b * .168f);
    sepia_filter.b = sat((float)no_filter.r * .272f + (float)no_filter.g * .543f + (float)no_filter.b * .131f);

    image_set_pixel(image, (point){x, y}, sepia_filter);
}

void sepia_one_pixel_by_idx(image* old_image, image* image, size_t i) {
    pixel no_filter = image_get_pixel_by_idx(old_image, i);
    pixel sepia_filter = image_get_pixel_by_idx(image, i);

    sepia_filter.r = sat((float)no_filter.r * .393f + (float)no_filter.g * .769f + (float)no_filter.b * .189f);
    sepia_filter.g = sat((float)no_filter.r * .349f + (float)no_filter.g * .686f + (float)no_filter.b * .168f);
    sepia_filter.b = sat((float)no_filter.r * .272f + (float)no_filter.g * .543f + (float)no_filter.b * .131f);

    image_set_pixel_by_idx(image, i, sepia_filter);
}

void image_make_sepia_c(image* image) {
    uint32_t width = image_get_width(image);
    uint32_t height = image_get_height(image);
    struct image* old_image = image_copy(image);

    for (uint32_t i = 0; i < width; i ++) {
        for (uint32_t j = 0; j < height; j ++) {
            sepia_one_pixel(old_image, image, i, j);
        }
    }
    image_destroy(old_image);
}

void image_make_sepia_asm(image* image) {
    uint32_t width = image_get_width(image);
    uint32_t height = image_get_height(image);
    struct image* old_image = image_copy(image);

    if (height * width < 4) {
        for (uint32_t i = 0; i < width; i ++) {
            for (uint32_t j = 0; j < height; j++) {
                sepia_one_pixel(old_image, image, i, j);
            }
        }
        return;
    }

    for (size_t i = 0; i < (height * width)/4*4 ; i+=4) {
        float src[12] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        pixel p0 = image_get_pixel_by_idx(old_image, i);
        pixel p1 = image_get_pixel_by_idx(old_image, i + 1);
        pixel p2 = image_get_pixel_by_idx(old_image, i + 2);
        pixel p3 = image_get_pixel_by_idx(old_image, i + 3);

        src[0] = p0.r; src[1] = p0.g; src[2] = p0.b;
        src[3] = p1.r; src[4] = p1.g; src[5] = p1.b;
        src[6] = p2.r; src[7] = p2.g; src[8] = p2.b;
        src[9] = p3.r; src[10] = p3.g; src[11] = p3.b;
        float dst[12] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        sepia_sse(src, dst);
        image_set_pixel_by_idx(image, i, (pixel){dst[0], dst[1], dst[2]});
        image_set_pixel_by_idx(image, i + 1, (pixel){dst[3], dst[4], dst[5]});
        image_set_pixel_by_idx(image, i + 2, (pixel){dst[6], dst[7], dst[8]});
        image_set_pixel_by_idx(image, i + 3, (pixel){dst[9], dst[10], dst[11]});
    }

    for (size_t i  = height * width - (height * width) % 4; i < height * width; ++i) {
        sepia_one_pixel_by_idx(old_image, image, i);
    }
}
