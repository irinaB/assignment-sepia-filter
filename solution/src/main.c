#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>

#include "../include/image.h"
#include "../include/image_io.h"
#include "../include/image_utils.h"

void print_help(char* cmd_name);
void print_load_err(image_load_status status);
void print_save_err(image_save_status status);

int main(int arg_num, char** args) {
    if (arg_num < 4) {
        puts("Write 2 files like this:");
        print_help(args[0]);
        return 0;
    }

    char* input = args[1];
    char* output = args[2];

    image* image;
    struct image* img_asm;

    image_load_status load_status = image_read_from_bmp(input, &image);
    image_read_from_bmp(input, &img_asm);

    if (load_status == IMAGE_LOAD_OK) {
        struct rusage r;
        struct timeval start;
        struct timeval end;
        getrusage(RUSAGE_SELF, &r);
        start = r.ru_utime;
        image_make_sepia_c(image);

        getrusage(RUSAGE_SELF, &r);
        end = r.ru_utime;
        long res = ((end.tv_sec - start.tv_sec) * 1000000L) + end.tv_usec - start.tv_usec;
        printf( "Time of execution sepia (with C): %ld\n", res);
        printf( "end.tv_sec: %ld\n", end.tv_sec);
        printf( "start.tv_sec: %ld\n", start.tv_sec);
        printf( "end.tv_usec: %ld\n", end.tv_usec);
        printf( "start.tv_usec: %ld\n", start.tv_usec);
        image_save_status save_status = image_save_to_bmp(output, image);

        getrusage(RUSAGE_SELF, &r);
        start = r.ru_utime;
        image_make_sepia_asm(img_asm);
        end = r.ru_utime;
        res = ((end.tv_sec - start.tv_sec) * 1000000L) + end.tv_usec - start.tv_usec;
        printf( "Time of execution sepia (asm): %ld\n", res);
        printf( "end.tv_sec: %ld\n", end.tv_sec);
        printf( "start.tv_sec: %ld\n", start.tv_sec);
        printf( "end.tv_usec: %ld\n", end.tv_usec);
        printf( "start.tv_usec: %ld\n", start.tv_usec);
        image_save_status save_status_asm = image_save_to_bmp(output, img_asm);

        if (save_status == IMAGE_SAVE_OK && save_status_asm == IMAGE_SAVE_OK) {
            puts("Completed successfully");
        } else {
            print_save_err(save_status);
        }
    } else {
        print_load_err(load_status);
    }

    return 0;
}

void print_load_err(image_load_status status) {
    if (status == IMAGE_LOAD_BPP_NOT_SUPPORTED)
        puts("Unsupported file size");
    else if (status == IMAGE_LOAD_COMPRESSION_NOT_SUPPORTED)
        puts("We don't support this file");
    else if (status == IMAGE_LOAD_FILE_NOT_EXIST)
        puts("File is not exist");
    else if (status == IMAGE_LOAD_TYPE_UNSUPPORTED)
        puts("Format file error");
    else if (status == IMAGE_LOAD_READ_FAIL)
        puts("Can't read file");
    else
        puts("Unnamed error");
}

void print_save_err(image_save_status status) {
    if (status == IMAGE_SAVE_NO_ACCESS)
        puts("Can't access to file");
    else if (status == IMAGE_SAVE_OPEN_FAIL)
        puts("Can't open file");
    else if (status == IMAGE_SAVE_WRITE_FAIL)
        puts("Can't write file");
    else
        puts("Unnamed error");
}

void print_help(char* cmd_name) {
    printf(
        "Using: %s <src> <dst> <degrees>\n"
        "input Input file\n"
        "output Output file\n",
        cmd_name
    );
}
